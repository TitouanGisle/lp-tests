<?php
declare(strict_types=1);

final class Adherent
{
    private $identifiant;

    private function __construct(string $nom, string $prenom, string $date)
    {   
        $this->validateBirthdate($date);
        $this->identifiant = $this->normalize($nom, $prenom, $date);
    }

    public function __toString(): string
    {
        return $this->identifiant;
    }

    private function validateBirthdate(string $date) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException(
                sprintf(
                    '"%s" is not a valid email address',
                    $email
                )
            );
        }
    }

    private function normalize(string $nom, string $prenom, string $date): void
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException(
                sprintf(
                    '"%s" is not a valid email address',
                    $email
                )
            );
        }
    }

    /**
     * Get the value of identifiant
     */ 
    public function getIdentifiant()
    {
        return $this->identifiant;
    }
}
