<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class AdherentTest extends TestCase
{
    public function testCanBeCreatedFromValidNames(): void
    {
        $this->assertInstanceOf(
            Adherent::class,
            new Adherent('Michel', 'Légende', '21-05-2001')
        );
    }

    /**
     * Vérifie que l'identifiant généré corresponde à la norme demandée
     */
    public function testGeneratesNormalizedId() : void
    {
        $adherent_test = new Adherent('Jean-Michel', 'Légende', '21-05-2001');

        $this->assertEquals(
            $adherent_test->getIdentifiant(), 
            'jean michel legende 21-05-2001'
        );
    }

    public function testCannotBeCreatedFromInvalidAdherentBirthdate(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new Adherent ('Jean-Michel', 'Légende', 'born to be wild');
    }
    
}
